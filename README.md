# Conway's Game of Life
**Harrison Nicholls (hen204@exeter.ac.uk)**\
A C implementation of Conway's Game of Life for final project in the PHY2027 Scientific Programming in C module.



__[Description from Wikipedia](https://en.wikipedia.org/wiki/Conway's_Game_of_Life):__\
The universe of the Game of Life is an infinite, two-dimensional orthogonal grid of square cells, each of which is in one of two possible states, alive or dead, (or populated and unpopulated, respectively). Every cell interacts with its eight neighbours, which are the cells that are horizontally, vertically, or diagonally adjacent.

## Game rules:
1. Any live cell with fewer than two live neighbours dies, as if by underpopulation.
2. Any live cell with `S` neighbours lives on to the next generation.
3. Any live cell with more than three live neighbours dies, as if by overpopulation.
4. Any dead cell with `B` neighbours is born.
The default rules choose `S = 3,2` and `B = 3`.

## Documentation:
The game can simply be launched by executing the binary.
You can also run with arguments...\
* `--dims <width> <height>` Sets the dimensions of the array of cells in terms of count in the x and y directions
* `--rules <S> <B>` Sets the game rules for that instance of the game, as explained above.
* `--play` Causes the game to start 'playing' on launch, rather than in a paused state.
* `--rainbow` Enables rainbow mode.

## Requirements for running the game:
* GLFW 3
* Driver support for OpenGL
	- OpenGL is deprecated on macOS on version 10.14, and later.
* A window system of some form (e.g. X11).
* GTK3 / Zenity if compiled with `#define USE_DIALOGS true`

## Building:
Run `make` in the directory containing `Makefile`, with `./src` as a subdirectory. The binary is called `conway`, and is stored in `./bin`.\
__Requirements:__\
* All previous requirements
* GCC
* Make

## Example output (compilation and run):
```
harrison@hplaptop ~/Documents/uni/Year 2/Scientific Programming in C/conways-game-of-life> make; ./bin/conway -p --dims 80 60 --rules 32 3
gcc -Wall -Wextra -g -I ./inc/ -I /usr/local/include -c src/main.c  -o src/main.o
gcc -Wall -Wextra -g -I ./inc/ -I /usr/local/include -c src/util.c  -o src/util.o
gcc -Wall -Wextra -g -I ./inc/ -I /usr/local/include -c src/loop.c  -o src/loop.o
gcc -Wall -Wextra -g -I ./inc/ -I /usr/local/include -c src/renderable.c  -o src/renderable.o
gcc -Wall -Wextra -g -I ./inc/ -I /usr/local/include -c src/fileio.c  -o src/fileio.o
rm -f ./bin/conway
gcc -Wall -Wextra -g -I ./inc/ -I /usr/local/include -o ./bin/conway ./src/main.o ./src/util.o ./src/loop.o ./src/renderable.o ./src/fileio.o -L /usr/local/lib -L ./lib/  -lglfw -lGL -lm 
rm -f ./src/*.o
printf "\n" 

Welcome to Conway's Game of Life. Implementation by Harrison Nicholls. 
Game is now playing. 
Game is now paused. 
Game is now playing. 
Game is now paused. 
File: '/home/harrison/Documents/uni/Year 2/Scientific Programming in C/conways-game-of-life/res/example.cws'
Done writing file.
Exiting...
Bye!
```

## Example screenshots:
Game on launch:\
![alt text](https://gitlab.com/harrisonnicholls13/conways-game-of-life/raw/master/res/img/ex1.png "Game on launch")\
Game after some evolution:\
![alt text](https://gitlab.com/harrisonnicholls13/conways-game-of-life/raw/master/res/img/ex2.png "Game after some evolution")\
Game after a long time:\
![alt text](https://gitlab.com/harrisonnicholls13/conways-game-of-life/raw/master/res/img/ex3.png "Game after a long time")\
Game with alternative rules:\
![alt text](https://gitlab.com/harrisonnicholls13/conways-game-of-life/raw/master/res/img/ex4.png "Game with rules S/B = 45/1256")\
