#include <conway.h>

// Function to draw rectangles to the screen using gl vertex
// x,y define the bottom left corner
void rectangle(float x, float y,float w, float h, float color[3]) {
	float xw = x+w; // optimisation, since this would otherwise be calculated twice
	float yh = y+h;

	glBegin(GL_QUADS);
	glColor3d(color[0],color[1],color[2]);
	// I don't know why this is the vertex order, but it doesn't really matter
    glVertex2f(x, yh); // top left
    glVertex2f(x, y); // bottom left
    glVertex2f(xw, y); // bottom right
	glVertex2f(xw, yh); // top right
    glEnd();

	/*
		1 ----- 4
		|	    |
		|       |
		2 ----- 3		
	*/
}

// Draw pause sign in the top left of the screen.
// The numbers here seem arbitrary since the parameters of each rectangle are defined by aesthetics.
void pause_sign(int wh) {
	float w = 40;
	float h = 40;
	float black[] = {0.2,0.2,0.2};
	float white[] = {1,1,1};
	
	rectangle(4,wh - 5 - h-1,w+2,h+2,white);
	rectangle(5,wh - 5 - h,w,h,black);
	rectangle(13,wh - 5 - h + 5,10,h-10,white);
	rectangle(28,wh - 5 - h + 5,10,h-10,white);
}

// Renders an indicator in the top right of the screen
// Indicates which placemode has been selected
void place_sign(int n, int ww, int wh){
	float h = 40; // height
	float w1 = 12; // width of each block
	float s = 2; // spacing between each block
	float w = s + (w1 + s) * 9 + s; // width of main bit
	float p = 2;  // padding
	float black[] = {0.2,0.2,0.2};
	float grey[] = {0.5,0.5,0.5};
	float white[] = {1,1,1};

	rectangle(ww-p-w-s-1,wh-p-h-1-2,w+2,h+2,white);
	rectangle(ww-p-w-s,wh-p-h-2,w,h,black);
	for (int i = 1; i <= 9; i++){
		if (i == n){
			rectangle(ww-p-w+s+(w1+s)*(i-1),wh-h+p/2,10,h-10,white);
		} else {
			rectangle(ww-p-w+s+(w1+s)*(i-1),wh-h+p/2,10,h-10,grey);
		}
	}
}
